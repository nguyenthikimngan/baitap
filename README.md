# baitap
2. hiện tượng callback hell là gì?
hiện tượng các callback lồng nhau, khiến cho cách code không tối ưu
3. phân biệt let và const
cả hai đều khai báo scope và có tính hosting
let cho phép cập nhật giá trị của biến chứ không cho phép chúng tái khai báo 
const phải khởi gán giá trị khi khai báo 
4. phân biệt forEach, filter, map ,every,some for thường?
-forEach lặp lại lại từng phần tử trong mảng 
- filter tạo 1 mảng mới bao gồm các phần tử trả về true và bỏ qua các phần tử trả về false
-map trả về 1 mảng đối tượng mới được tạo bằng cách thực hiện 1 số hành động trên mục gốc 
- every trả về boolean true nếu mọi phần tử trong mảng thỏa mãn và ngược lại 
-some trả về true nếu có ít nhất một phần tử trong mảng thỏa mãn và ngược lại 
5. phân biệt địa chỉ và giá trị của biến?
địa chỉ biến là chuỗi định dạng ô nhớ lưu trữ dữ liệu 
giá trị của biến là lưu trữ giá trị các dữ liệu hay các đối tượng 
7. phân biệt call,bind,apply?
-call,apply,bind là các prototype của function
+call gọi hàm và cho phép truyền vào 1 object các đối số phân cách nhau bởi dấu phẩy
+apply gọi hàm cho phép truyền vào 1 object và các đối số qua mảng 
+bind trả về 1 hàm số mới cho phép truyền vào 1 object và các đối số phân cách nhau bằng dấu phẩy
8. javascript có bao nhiêu kiểu dữ liệu?
-primitives: number,string,boolean
-special types: undefined,null
-reference type: object,array
9. prototype là gì?
là cơ chế mà các object trong javascript kế thừa các tính năng từ 1 object khác 
10. các phương pháp để khởi tạo object?
sử dụng object literals
sử dụng object contructor function``
11. trình bày các phương pháp để clone object?
sử dụng spread
sử dụng object.assign
sử dụng json 


