const student=[{
    id : 1,
    name: 'nghia',
    age:26,
    subject:[
        'toán',
        'lý',
        'hóa',
    ],
    type: 'giỏi',
    tien:10000,

},
{
    id:2,
    name: 'ngan',
    age:23,
    subject:[
        'toán',
        'lý',
        'hóa',
    ],
    type:'giỏi',
    tien: 10000,

},
{
    id:3,
    name: 'thang',
    age:28,
    subject:[
        'toán',
        'lý',
        'hóa',
    ],
    type:'khá',
    tien: 10000,

},
{
    id:4,
    name: 'truong',
    age:20,
    subject:[
        'toán',
        'lý',
        'hóa',
    ],
    type:'khá',
    tien: 10000,

},
]

// get ra danh sách tên sinh viên 
const listName= student.map((student)=>
student.name);

// get ra danh sách tên sinh viên lớn hơn 25t
const filterstudent = student.filter(function(student){
  return student .age >25;
});
//get ra danh sách tên sinh viên nhỏ hơn 25 t và loại giỏi 
const resultstudent = student.filter ((student)=>student.age<25 && student.type ==="giỏi").map ((student)=>student.name);
// tính tổng tiền của sinh viên
const totaltien = student.reduce(function(accumulator,currentValue){
    return accumulator + currentValue.tien;
},0);
// tính tổng tiền của sinh viên lớn hơn 25 t
const totaltien2 = student.filter(student=>student.age>25).reduce((totaltien2,student)=>totaltien2+student.tien,0);
//in ra chuỗi chứa tên sinh viên theo format name1-name2 
const Student = student.map((student)=>student.name).join('-');
// get ra danh sách tên sinh viên học giỏi và có tham gia lớp toán 
const Student = student .filter((student)=>student.type ==="giỏi"&& student.subject.includes("toán")).map ((student)=>student.name);    